package com.example.demo.controllers;

import com.example.demo.model.persistence.Cart;
import com.example.demo.model.persistence.Item;
import com.example.demo.model.persistence.User;
import com.example.demo.model.persistence.repositories.CartRepository;
import com.example.demo.model.persistence.repositories.ItemRepository;
import com.example.demo.model.persistence.repositories.UserRepository;
import com.example.demo.model.requests.ModifyCartRequest;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.util.Optional;

import static org.junit.Assert.*;
import static org.mockito.BDDMockito.given;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CartControllerTest {

    @Autowired
    CartController cartController;

    @MockBean
    UserRepository userRepository;

    @MockBean
    ItemRepository itemRepository;

    @MockBean
    CartRepository cartRepository;

    @Test
    public void addToCart_happyPath() {

        ModifyCartRequest cartRequest = getCartRequest();
        given(itemRepository.findById(cartRequest.getItemId()))
                .willReturn(Optional.of(getTestItem()));

        given(userRepository.findByUsername(cartRequest.getUsername()))
                .willReturn(getTestUserWithEmptyCart());

        final ResponseEntity<Cart>
                response = cartController.addToCart(cartRequest);

        assertNotNull(response);
        assertEquals(200,response.getStatusCodeValue());

        Cart cart = response.getBody();
        assertNotNull(cart);
        assertEquals(5, cart.getItems().size()); // there are 5 items in the request
    }

    @Test
    public void addToCart_unhappyPath_no_cart_present() {

        ModifyCartRequest cartRequest = getCartRequest();
        given(itemRepository.findById(cartRequest.getItemId()))
                .willReturn(Optional.of(getTestItem()));

        given(userRepository.findByUsername(cartRequest.getUsername()))
                .willReturn(getTestUserWithoutCart());

        final ResponseEntity<Cart>
                response = cartController.addToCart(cartRequest);

        assertNotNull(response);
        assertEquals(404,response.getStatusCodeValue()); //no cart found for user

    }

    @Test
    public void addToCart_unhappyPath_no_user_present() {

        ModifyCartRequest cartRequest = getCartRequest();
        given(itemRepository.findById(cartRequest.getItemId()))
                .willReturn(Optional.of(getTestItem()));

        given(userRepository.findByUsername(cartRequest.getUsername()))
                .willReturn(null);

        final ResponseEntity<Cart>
                response = cartController.addToCart(cartRequest);

        assertNotNull(response);
        //no cart found, because there is no user
        assertEquals(404,response.getStatusCodeValue());

    }


    @Test
    public void removeFromCart_happyPath() {

        ModifyCartRequest cartRequest = getCartRequest();
        given(itemRepository.findById(cartRequest.getItemId()))
                .willReturn(Optional.of(getTestItem()));

        given(userRepository.findByUsername(cartRequest.getUsername()))
                .willReturn(getTestUserWithFullCart());

        final ResponseEntity<Cart>
                response = cartController.removeFromCart(cartRequest);

        assertNotNull(response);
        assertEquals(200,response.getStatusCodeValue());

        Cart cart = response.getBody();
        assertNotNull(cart);

        // all items in the cart should be removed by the request
        assertEquals(0, cart.getItems().size());

    }

    @Test
    public void removeFromCart_unhappyPath() {

        ModifyCartRequest cartRequest = getCartRequest();
        given(itemRepository.findById(cartRequest.getItemId()))
                .willReturn(Optional.of(getTestItem()));

        given(userRepository.findByUsername(cartRequest.getUsername()))
                .willReturn(getTestUserWithoutCart());

        final ResponseEntity<Cart>
                response = cartController.removeFromCart(cartRequest);

        assertNotNull(response);
        assertEquals(404,response.getStatusCodeValue());

    }

    /**
     * TestItem for Cart
     * @return Object of type {@link Item}
     */
    private Item getTestItem() {
        return new Item(
                "TestItem",
                new BigDecimal(10.00),
                "for test purpose"
        );
    }

    /**
     * TestRequest Object for adding to cart
     * @return Object of type {@link ModifyCartRequest}
     */
    private ModifyCartRequest getCartRequest() {
        return new ModifyCartRequest(
                "Testuser",
                1L,
                5
        );
    }

    /**
     * create a test User with a cart
     * @return object of type {@link User}
     */
    private User getTestUserWithEmptyCart() {

        User user = new User("Testuser","topsecret");
        Cart cart = new Cart();
        user.setCart(cart); // add empty cart to user

        return user;
    }

    private User getTestUserWithFullCart() {

        User user = new User("Testuser","topsecret");
        Cart cart = new Cart();
        // Add 5 Testitems
        for (int i = 0; i < 5; i++) {
            cart.addItem(getTestItem());
        }
        user.setCart(cart); // add empty cart to user
        return user;
    }

    private User getTestUserWithoutCart() {

        User user = new User("Testuser","topsecret");
        Item item = getTestItem();

        return user;
    }
}
