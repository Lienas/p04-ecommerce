package com.example.demo.controllers;

import com.example.demo.model.persistence.Cart;
import com.example.demo.model.persistence.Item;
import com.example.demo.model.persistence.User;
import com.example.demo.model.persistence.UserOrder;
import com.example.demo.model.persistence.repositories.OrderRepository;
import com.example.demo.model.persistence.repositories.UserRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;

@RunWith(SpringRunner.class)
@SpringBootTest
public class OrderControllerTest {
    @Autowired
    OrderController orderController;

    @MockBean
    UserRepository userRepository;

    @MockBean
    OrderRepository orderRepository;


    @Test
    public void submit_happyPath() {

        String username = "TestUser";
        given(userRepository.findByUsername(username))
                .willReturn(getTestUserWithCart());

        final ResponseEntity<UserOrder>
                response = orderController.submit(username);

        assertNotNull(response);
        assertEquals(200, response.getStatusCodeValue());

        UserOrder order = response.getBody();

        assertNotNull(order);
        assertEquals(2, order.getItems().size());
    }

    @Test
    public void submit_unhappyPath() {

        String username = "Testuser";
        given(userRepository.findByUsername(username))
                .willReturn(null);

        final ResponseEntity<UserOrder>
                response = orderController.submit(username);

        assertNotNull(response);

        // no user found
        assertEquals(404, response.getStatusCodeValue());

    }


    @Test
    public void getOrdersForUser_happyPath() {

        String username = "Testuser";
        given(userRepository.findByUsername(username))
                .willReturn(getTestUserWithCart());

        given(orderRepository.findByUser(any()))
                .willReturn(getTestOrderHistoryForUser());

        final ResponseEntity<List<UserOrder>>
                response = orderController.getOrdersForUser(username);

        assertNotNull(response);
        assertEquals(200, response.getStatusCodeValue());

        final List<UserOrder> orders = response.getBody();
        assertNotNull(orders);
        assertEquals(1, orders.size());

    }

    /**
     * create a testuser with some item in his cart
     *
     * @return object of type {@link User}
     */
    private User getTestUserWithCart() {

        User user = new User("Testuser", "topsecret");
        Cart cart = new Cart();
        // Add 2 Testitems
        Item item = new Item(
                "TestItem",
                new BigDecimal(10.00),
                "for test purpose");

        for (int i = 0; i < 2; i++) {
            cart.addItem(item);
        }
        user.setCart(cart);
        return user;
    }

    /**
     * create a orderhistory for test
     *
     * @return List of {@link UserOrder}
     */
    private List<UserOrder> getTestOrderHistoryForUser() {

        List<UserOrder> orders = new LinkedList<>();
        UserOrder order = UserOrder.createFromCart(getTestUserWithCart().getCart());

        orders.add(order);

        return orders;
    }
}
