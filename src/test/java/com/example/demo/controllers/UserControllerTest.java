package com.example.demo.controllers;

import com.example.demo.model.persistence.User;
import com.example.demo.model.persistence.repositories.CartRepository;
import com.example.demo.model.persistence.repositories.UserRepository;
import com.example.demo.model.requests.CreateUserRequest;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Optional;

import static org.mockito.BDDMockito.given;
import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserControllerTest {

    @Autowired
    private UserController userController;

    @MockBean
    private UserRepository userRepository;

    @MockBean
    private CartRepository cartRepository;

    @MockBean
    BCryptPasswordEncoder passwordEncoder;

    @Test
    public void findById_happy_Path() {
        Long id = 1L;
        given(userRepository.findById(id))
                .willReturn(java.util.Optional.of(getUser()));

        ResponseEntity<User>
                response = userController.findById(id);

        assertNotNull(response);
        assertEquals(200, response.getStatusCodeValue());

        User user = response.getBody();
        assertNotNull(user);

    }

    @Test
    public void findById_unhappy_Path() {
        Long id = 1L;
        given(userRepository.findById(id))
                .willReturn(Optional.empty());

        ResponseEntity<User>
                response = userController.findById(id);

        assertNotNull(response);
        assertEquals(404, response.getStatusCodeValue()); // not found

        User user = response.getBody();
        assertNull(user);

    }


    @Test
    public void findByUserName_happy_path() {

        String username = "Testuser";
        given(userRepository.findByUsername(username))
                .willReturn(getUser());

        final ResponseEntity<User>
                response = userController.findByUserName(username);

        assertNotNull(response);
        assertEquals(200, response.getStatusCodeValue());

        User user = response.getBody();
        assertNotNull(user);
        assertEquals(0, user.getId());
        assertEquals("Testuser", user.getUsername());
        assertEquals("tercespot", user.getPassword());

    }

    @Test
    public void findByUserName_unhappy_path() {

        String username = "Test"; // should be Testuser
        given(userRepository.findByUsername(username))
                .willReturn(null);
        final ResponseEntity<User>
                response = userController.findByUserName(username);

        assertNotNull(response);
        assertEquals(404, response.getStatusCodeValue()); // not found

    }

    @Test
    public void createUser_happy_path() {

        given(passwordEncoder.encode("topsecret"))
                .willReturn("tercespot");

        CreateUserRequest userRequest = new CreateUserRequest();
        userRequest.setUsername("Testuser");
        userRequest.setPassword("topsecret");
        userRequest.setConfirmPassword("topsecret");

        final ResponseEntity<User>
                response = userController.createUser(userRequest);

        assertNotNull(response);
        assertEquals(200, response.getStatusCodeValue());

        User user = response.getBody();
        assertNotNull(user);
        assertEquals(0, user.getId());
        assertEquals("Testuser", user.getUsername());
        assertEquals("tercespot", user.getPassword());

    }

    @Test
    public void createUser_unhappy_path() {

        CreateUserRequest userRequest = new CreateUserRequest();

        userRequest.setUsername("Testuser");
        userRequest.setPassword("topsecret");
        userRequest.setConfirmPassword("secret"); // not correct !

        final ResponseEntity<User>
                response = userController.createUser(userRequest);

        assertNotNull(response);
        assertEquals(400, response.getStatusCodeValue()); // should be bad request

    }

    /**
     * Create an example User for testing
     *
     * @return example User Object
     */
    private User getUser() {
        User user = new User();
        user.setUsername("Testuser");
        user.setPassword("tercespot");

        return user;
    }

}
