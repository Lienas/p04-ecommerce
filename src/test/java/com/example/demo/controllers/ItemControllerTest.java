package com.example.demo.controllers;

import com.example.demo.model.persistence.Item;
import com.example.demo.model.persistence.repositories.ItemRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.*;
import static org.mockito.BDDMockito.given;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ItemControllerTest {

    @Autowired
    ItemController itemController;

    @MockBean
    ItemRepository itemRepository;

    @Test
    public void getItems_happy_path() {
        given(itemRepository.findAll()).willReturn(getListOfItems());
        final ResponseEntity<List<Item>> response = itemController.getItems();

        assertNotNull(response);
        assertEquals(200, response.getStatusCodeValue());

        final List<Item> items = response.getBody();

        assertNotNull(items);
        assertEquals(3,items.size());
    }

    @Test
    public void getItemById_happy_path() {
        final Long id = 1L;

        given(itemRepository.findById(id))
                .willReturn(Optional.ofNullable(getListOfItems().get(0)));

        final ResponseEntity<Item>
                response = itemController.getItemById(id);

        assertNotNull(response);
        assertEquals(200, response.getStatusCodeValue());

        final Item item = response.getBody();
        assertNotNull(item);
        assertEquals("item1", item.getName());

    }

    @Test
    public void getItemById_unhappy_path() {
        final Long id = 1L;

        given(itemRepository.findById(id))
                .willReturn(Optional.empty());

        final ResponseEntity<Item>
                response = itemController.getItemById(id);

        assertNotNull(response);
        assertEquals(404, response.getStatusCodeValue()); // not found

        final Item item = response.getBody();
        assertNull(item);

    }

    @Test
    public void getItemsByName_happyPath() {
        final String name = "test1";

        given(itemRepository.findByName(name))
                .willReturn(getListOfItems().subList(0,1));

        final ResponseEntity<List<Item>>
                response = itemController.getItemsByName(name);

        assertNotNull(response);
        assertEquals(200, response.getStatusCodeValue());

        final List<Item> items = response.getBody();

        assertNotNull(items);
        assertEquals(1,items.size());
    }

    @Test
    public void getItemsByName_unhappyPath() {
        final String name = "test1";

        given(itemRepository.findByName(name))
                .willReturn(getListOfItems().subList(0,0)); // empty list

        final ResponseEntity<List<Item>>
                response = itemController.getItemsByName(name);

        assertNotNull(response);
        assertEquals(404, response.getStatusCodeValue()); // not found

        final List<Item> items = response.getBody();

        assertNull(items);
    }


    private List<Item> getListOfItems() {
        List<Item> itemsList = new ArrayList<>();
        itemsList.add(new Item("item1", BigDecimal.valueOf(1.01), "just for testing"));
        itemsList.add(new Item("item2", BigDecimal.valueOf(5.51), "just for testing"));
        itemsList.add(new Item("item3", BigDecimal.valueOf(111.00), "just for testing"));
        return itemsList;
    }
}
